
// Mad Lib
// Justin Rankin
// Paul Haller
// Mason Brull

#include <iostream>
#include <conio.h>
#include <fstream>

using namespace std;

void Print(string answers[])
{
	cout << "\n One day my " + answers[0] + " friend and I decided to go to the " + answers[1] + " game in " + answers[2] + ".\n";
	cout << "We really wanted to see " + answers[3] + " play.\n";
	cout << "So we " + answers[4] + " in the " + answers[5] + " and headed down to the " + answers[6] + " and bought some " + answers[7] + ".\n";
	cout << "We watched the game and it was " + answers[8] + ".\n";
	cout << "We ate some " + answers[9] + " and drank some " + answers[10] + ".\n";
	cout << "We had a " + answers[11] + " time, and can't wait to go again.\n";
}

int main()
{
	string questions[12] = {
		{ "Enter adjective : "},
		{"Enter a sport : "},
		{"Enter a city : "},
		{"Enter a person : "},
		{"Enter an action verb : "},
		{"Enter a vehicle : "},
		{"Enter a place : "},
		{"Enter a noun : "},
		{"Enter an adjective : "},
		{"Enter a food : "},
		{"Enter a liquid : "},
		{"Enter a adjective : "}
	};

	string answers[12];

	for (int i = 0; i < 12; i++)
	{
		cout << questions[i];
		cin >> answers[i];
	}

	Print(answers);

	char input;
	cout << "Would you like save to a text file?(y/n) \n";

	if (input = 'y')
	{
		string filepath = "Test.text";
		ofstream ofs(filepath);
		if (ofs)
		{
			ofs << "\nOne day my " + answers[0] + " friend and I decided to go to the " + answers[1] + " game in " + answers[2] + ".\n";
			ofs << "We really wanted to see " + answers[3] + " play.\n";
			ofs << "So we " + answers[4] + " in the " + answers[5] + " and headed down to the " + answers[6] + " and bought some " + answers[7] + ".\n";
			ofs << "We watched the game and it was " + answers[8] + ".\n";
			ofs << "We ate some " + answers[9] + " and drank some " + answers[10] + ".\n";
			ofs << "We had a " + answers[11] + " time, and can't wait to go again.\n";
		}
	}




	(void)_getch();
	return 0;
}

